// A simple test to exercise PWM, Tone and Servo functions simultaneously
// This code is released to the public domain with no guarantees whatsoever
// "If it breaks in half, you get to keep both halves." ~ George Carlin

#include <user_interface.h>  // so we can power down the modem & minimize noise / current
#include <Servo.h>

const int CH1pin = 2;  // D4, GPIO2 for PWM channel 1 (always running if duty cycle > 0)
const int CH2pin = 5;  // D1, GPIO5 for PWM channel 2 (inverse of channel 1 if enabled)
const int TONEpin = 12;  // D6, GPIO12 for a tone output
const int SERVO1pin = 13;  // D7, GPIO13 for the first servo
const int SERVO2pin = 15;  // D8, GPIO15 for the second servo
const int TP1 = 4;  // D2, GPIO4 test point, toggles with each PWM inc/dec loop
const int TP2 = 14;  // D5, GPIO14 test point, toggles with each PWM change

String command;  // stores the requested command from the serial monitor
uint32_t delayDutyCycle = 10;  // delay between duty cycle changes
uint32_t freqPWM = 1000;  // initial PWM frequency
uint32_t dutyCycle;  // global so we can see it in the command processor
uint32_t minDutyCycle = 1;  // minimum duty cycle
uint32_t maxDutyCycle = 1022;  // maximum duty cycle
uint32_t loopDelay = 0;  // delay between INC/DEC loops
bool twoChannels = true;  // one or two channel PWM output
bool hold = false;  // if true stops the INC/DEC cycling
uint32_t toneFreq = 0;  // default no tone output
uint32_t servoAngle = 90;  // default servo set to neutral but not enabled

Servo servo1;
Servo servo2;

void setup() {
  Serial.begin(74880);  // start serial for output
  Serial.println();  // clear the boot message
  Serial.println(F("PWM / Tone / Servo test program (press ? followed by <enter> for help)"));
  Serial.println();
  analogWriteFreq(freqPWM);
  pinMode(TP1, OUTPUT); // test point for outer loop
  pinMode(TP2, OUTPUT); // test point for duty cycle changes
  wifi_set_opmode_current(NULL_MODE);  // set Wi-Fi working mode to unconfigured, don't save to flash
  wifi_fpm_set_sleep_type(MODEM_SLEEP_T);  // set the sleep type to modem sleep
  wifi_fpm_open();  // enable Forced Modem Sleep
  wifi_fpm_do_sleep(0xFFFFFFF);  // force the modem to enter sleep mode
  delay(5);  // the modem sleeps during this delay()
}

void loop() {

  digitalWrite(TP1, HIGH);  // toggle the activity LED
  // increase duty cycle, decrease for second channel
  for (dutyCycle = minDutyCycle; dutyCycle < maxDutyCycle + 1; dutyCycle++) {
    digitalWrite(TP2, !digitalRead(TP2));  // toggle around duty cycle changes
    commandCheck();
    while (hold)
    {
      commandCheck();
      delay(10);
    }
    if (twoChannels)
      analogWrite(CH2pin, (1022 + 1) - dutyCycle);
    analogWrite(CH1pin, dutyCycle);
    delay(delayDutyCycle);
  }
  delay(loopDelay);

  digitalWrite(TP1, LOW);  // toggle the activity LED
  // decrease duty cycle, increase for second channel
  for (dutyCycle = maxDutyCycle - 1; dutyCycle > minDutyCycle; dutyCycle--) {
    digitalWrite(TP2, !digitalRead(TP2));  // toggle around duty cycle changes
    commandCheck();
    while (hold)
    {
      commandCheck();
      delay(10);
    }
    if (twoChannels)
      analogWrite(CH2pin, (1022 + 1) - dutyCycle);
    analogWrite(CH1pin, dutyCycle);
    delay(delayDutyCycle);
  }
  delay(loopDelay);
}

void commandCheck() {
  if (Serial.available()) {                    // serial monitor command processor
    command = Serial.readStringUntil('\n');    // strip the newline
    command.trim();
    if (command.equalsIgnoreCase("help") || command.equalsIgnoreCase("?"))   // available commands list
    {
      Serial.println(F("available commands:"));
      Serial.println(F("2000f sets PWM frequency to 2000Hz, doesn't update until you change the duty cycle"));
      Serial.println(F("c toggles single channel / two channel PWM mode"));
      Serial.println(F("10p sets the pause between duty cycle changes to 10ms"));
      Serial.println(F("30m sets PWM minimum duty cycle to 30/1022"));
      Serial.println(F("900x sets PWM maximum duty cycle to 900/1022"));
      Serial.println(F("200L adds a 200ms delay between sweep loops"));
      Serial.println(F("h toggles holding current duty cycle or continues the PWM sweep"));
      Serial.println(F("+ increments the duty cycle while in HOLD"));
      Serial.println(F("- decrements the duty cycle while in HOLD"));
      Serial.println(F("300d set the duty cycle of CH1 to 300 while in HOLD"));
      Serial.println(F("500t plays a 500Hz tone, 0t or t stops the tone"));
      Serial.println(F("90s sets the 1st servo to 90 degrees, > 180 or 's' detaches the servo"));
      Serial.println(F("90a sets the 2nd servo to 90 degrees, > 180 or 'a' detaches the servo"));
      Serial.println();
    } else {

      switch ( command.charAt(command.length() - 1) ) {  // get the last byte (the requested command)

        case 'c': case 'C':         // C toggles 1-channel / 2 channel PWM
          twoChannels = !twoChannels;
          Serial.print(F("running "));
          if (twoChannels) {
            Serial.println(F("2 PWM channels"));
          } else {
            Serial.println(F("1 PWM channel"));
            analogWrite(CH2pin, 0);
          }
          break;

        case 'h': case 'H':         // H freezes at current duty cycle, or free-runs the sweep loops
          hold = !hold;
          if (hold) {
            Serial.print(F("holding PWM at current duty cycle = "));
            Serial.println(dutyCycle);
          } else {
            Serial.println(F("incrementing / decrementing PWM duty cycle"));
          }
          break;

        case '+':                   //  + increment the duty cycle in HOLD, min / max ignored
          if (dutyCycle < 1022)
            dutyCycle++;
          Serial.print(F("inc duty cycle of CH1 to "));
          Serial.print(dutyCycle);
          if (twoChannels){
            analogWrite(CH2pin, (1022 + 1) - dutyCycle);
          Serial.print(F("  duty cycle of CH2 set to "));
          Serial.print((1022 + 1) - dutyCycle);
          }
          analogWrite(CH1pin, dutyCycle);
          Serial.println();
          break;

        case '-':                   // - decrement the duty cycle in HOLD, min / max ignored
          if (dutyCycle > 1)
            dutyCycle--;
          Serial.print(F("dec duty cycle of CH1 to "));
          Serial.print(dutyCycle);
          if (twoChannels){
            analogWrite(CH2pin, (1022 + 1) - dutyCycle);
          Serial.print(F("  duty cycle of CH2 set to "));
          Serial.print((1022 + 1) - dutyCycle);
          }
          analogWrite(CH1pin, dutyCycle);
          Serial.println();
          break;

        case 'd': case 'D':         // 300d sets the duty cycle for CH1 to 300 in HOLD, min / max ignored
          command.remove(command.length() - 1);
          dutyCycle = (uint32_t) (command.toInt());
          if (dutyCycle > 1023)
            dutyCycle = 1023;
          Serial.print(F("duty cycle of CH1 set to "));
          Serial.print(dutyCycle);
          if (twoChannels){
            analogWrite(CH2pin, (1022 + 1) - dutyCycle);
          Serial.print(F("  duty cycle of CH2 set to "));
          Serial.print((1022 + 1) - dutyCycle);
          }
          analogWrite(CH1pin, dutyCycle);
          Serial.println();
          break;

        case 'f': case 'F':         // 100f sets the PWM frequency to 100Hz, default 1KHz
          command.remove(command.length() - 1);
          freqPWM = (uint32_t) (command.toInt());
          analogWriteFreq(freqPWM);
          Serial.print(F("PWM frequency "));
          Serial.print(freqPWM);
          Serial.println("Hz");
          break;

        case 'p': case 'P':         // 10p sets the pause between duty cycle changes to 10ms
          command.remove(command.length() - 1);
          delayDutyCycle = (uint32_t) (command.toInt());
          Serial.print(F("delay between duty cycle changes "));
          Serial.print(delayDutyCycle);
          Serial.println(" ms");
          break;

        case 'l': case 'L':         // 100L sets the loopDelay to 100 ms between tests
          command.remove(command.length() - 1);
          loopDelay = (unsigned int) (command.toInt());
          Serial.print(F("delay between PWM inc/dec test loops "));
          Serial.print(loopDelay);
          Serial.println("ms");
          break;

        case 'm': case 'M':         // 10m sets the minimum PWM duty cycle to 10/1022
          command.remove(command.length() - 1);
          minDutyCycle = (unsigned int) (command.toInt());
          Serial.print(F("minimum duty cycle "));
          Serial.println(minDutyCycle);
          break;

        case 'x': case 'X':         // 900x sets the maximum PWM duty cycle to 900/1022
          command.remove(command.length() - 1);
          maxDutyCycle = (unsigned int) (command.toInt());
          Serial.print(F("maximum duty cycle "));
          Serial.println(maxDutyCycle);
          break;

        case 't': case 'T':         // 500t plays a 500Hz tone, 0t or t stops the tone
          command.remove(command.length() - 1);
          toneFreq = (unsigned int) (command.toInt());
          if (toneFreq == 0) {
            noTone(TONEpin);
            Serial.println(F("tone stopped"));
          } else {
            tone(TONEpin, toneFreq);
            Serial.print(toneFreq);
            Serial.println(F("Hz tone output"));
          }
          break;

        case 's': case 'S':         // 90s puts the first servo at 90 degrees (neutral)
          command.remove(command.length() - 1);
          servoAngle = (unsigned int) (command.toInt());
          if ((servoAngle > 180) || (command.length() == 0)) {
            servo1.detach();
            Serial.println(F("servo1 detached"));
          } else {
            servo1.attach(SERVO1pin);
            servo1.write(servoAngle);
            Serial.print(F("servo1 angle ~ "));
            Serial.print(servoAngle);
            Serial.println(F(" degrees"));
          }
          break;

        case 'a': case 'A':         // 90a puts the second servo at 90 degrees (neutral)
          command.remove(command.length() - 1);
          servoAngle = (unsigned int) (command.toInt());
          if ((servoAngle > 180) || (command.length() == 0)) {
            servo2.detach();
            Serial.println(F("servo2 detached"));
          } else {
            servo2.attach(SERVO2pin);
            servo2.write(servoAngle);
            Serial.print(F("servo2 angle ~ "));
            Serial.print(servoAngle);
            Serial.println(F(" degrees"));
          }
          break;

        default:
          break;
      }
    }
  }
}
